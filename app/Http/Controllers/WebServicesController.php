<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Session;
use App\User;
use App\Mail\SendMail;
use App\Traits\FormRepeat;

class WebServicesController extends Controller
{
  use FormRepeat;
  public function create(Request $request){

    $validator = $this->validateFields($request);

    if ($validator->fails()) {
            //return response()->json(['status' => '0', 'message' => $validator->messages()->all()]);
            return response()->json(['status'=>'0','message'=>"<ul><li>".implode('</li><li>', $validator->messages()->all())."</li></ul>"]);
    }
    else{

      $u=$this->insertData($request);
      return response()->json(['status'=> '1','message'=>'sucessfully inserted entry']);
    }
  }

}
