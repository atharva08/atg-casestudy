<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Session;
use App\User;
use App\Mail\SendMail;
use App\Traits\FormRepeat;

class ATGController extends Controller
{
    use FormRepeat;
    public function form(){
    return view('form');
  }

  public function form_action(Request $request){

    $validator = $this->validateFields($request);


    if ($validator->fails()) {
            $error = "<ul><li>".implode('</li><li>', $validator->messages()->all())."</li></ul>";
            Session::flash('error', $error);
            return redirect()->back()->withInput();
    }
    else{
      $this->insertData($request);
      return redirect('/')->with('success','Record created successfully!');
  }
}

//
public function view_records(){
  $users = User::all();
  return view('records')->with(['users'=>$users]);
}



}
