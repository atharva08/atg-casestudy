<?php
namespace App\Traits;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendMail;

trait FormRepeat
{
    public function insertData($request) {
      $data = array(
        'email'=> $request->email
        );
      Mail::to(request('email'))->send(new SendMail($data));
      Log::notice('Email Sent');
      $u=new User;
      $u->name=request('name');
      $u->email=request('email');
      $u->pincode=request('pincode');
      $u->save();
      return $u;

    }

    public function validateFields(Request $request) {
      $validator = Validator::make($request->all(), [
             'name' => 'required|regex:/(^[A-Za-z ]+$)+/',
             'email' => 'required|unique:users|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
             'pincode' => 'required|numeric|digits:6'
      ]);
      return $validator;
    }

}
