<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Register</title>
  </head>
  <body>
    <div style="padding-left:35%;padding-top:8%">
      <form style="width: 50%" id="Form" >
        {{ csrf_field()}}

         @include('flash-message')

         <div id='messages'></div>
        <div class="text-center">
          <h3>Form</h3>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Name</label>
          <input type="text" name="name" class="form-control"  placeholder="Enter Name" value="{{old('name')}}" >
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input  name="email" class="form-control"  aria-describedby="emailHelp" placeholder="Enter email" value="{{old('email')}}">
        </div>

        <div class="form-group">
          <label for="exampleInputPincode">Pincode</label>
          <input type="text" name="pincode" class="form-control" placeholder="Enter pincode" value="{{old('pincode')}}">
        </div>
        <div class="form-group text-center">
            <button type="submit"  class="btn btn-primary">Submit</button>
        </div>

      </form>
      <form class="text-center" method="get" style="width: 50%" action="/form/view_records/">
        <button type="submit" class="btn btn-info">View Records</button>
      </form>
    </div>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
    <script>
    $(document).ready(function(){
    $('#Form').submit(function(){
    //console.log('hii');
    // Call ajax for pass data to other place
    $.ajax({
    type: 'POST',
    url: 'http://localhost:8000/api/user',
    data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(data){ // if getting done then call.
      //console.log(data.status);
      if (data.status==='0') {
        var error="<div class='alert alert-danger alert-dismissible fade show'>"+
                  "<strong>"+data.message+"</strong>" +
                  "<button type='button' class='close' data-dismiss='alert'>&times;</button>"+
                 "</div>";
        document.getElementById('messages').innerHTML=error;
      }
      if(data.status==='1'){
        //console.log('hiiiiii');
          var error="<div class='alert alert-success alert-dismissible fade show'>"+
                    "<strong>"+data.message+"</strong>" +
                    "<button type='button' class='close' data-dismiss='alert'>&times;</button>"+
                   "</div>";
          document.getElementById('response').innerHTML=error;
      }




    })
    .fail(function() { // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
      });
      // to prevent refreshing the whole page page
      return false;
      });
      });
    </script>
  </body>
</html>
